# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd

from tweet_analysis.tweet_opinion import analyze_opinion
from twitter_collect.collect_candidate_actuality_tweets import get_candidate_queries, \
    get_tweets_from_candidates_search_queries
from twitter_collect.collect_candidate_tweet_activity import candidate_tweets
from twitter_collect.to_dataframe import transform_to_dataframe
from twitter_collect.twitter_connection_setup import twitter_setup
from dash.dependencies import Input, Output


def generate_html(candidate_username, num_candidate):
    # Generate data from candidate
    api = twitter_setup()
    queries = get_candidate_queries(num_candidate, "../CandidateData")
    results_queries = get_tweets_from_candidates_search_queries(queries, api)
    data_queries = transform_to_dataframe(results_queries)
    pos_tweets, neu_tweets, neg_tweets = analyze_opinion(data_queries)

    results = candidate_tweets(candidate_username)
    data = transform_to_dataframe(results)

    # Graphical parameters
    pie_colors = ['#27a768', '#673ab7', '#de5347']

    html_element = html.Div(children=[
        html.Div(children=
        dcc.Graph(
            id='likes-rts-time',
            figure={
                'data': [
                    go.Scatter(
                        x=data['date'],
                        y=data['likes'],
                        mode='lines',
                        name='Likes'
                    ),
                    go.Scatter(
                        x=data['date'],
                        y=data['RTs'],
                        mode='lines',
                        name='Retweets'
                    )
                ],

                'layout': go.Layout(
                    xaxis={'title': 'Timeline'},
                    yaxis={'title': 'Quantity'},
                    margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
                    legend={'x': 0, 'y': 1},
                    hovermode='closest'
                )
            }
        ), className="four.columns"
        ),

        html.Div(children=
        dcc.Graph(
            id='pos-neu-neg-tweets',
            figure={
                'data': [
                    go.Pie(labels=['Positive', 'Neutral', 'Negative'],
                           values=[len(pos_tweets), len(neu_tweets), len(neg_tweets)],
                           hoverinfo='label+percent', textinfo='value',
                           textfont=dict(size=20),
                           marker=dict(colors=pie_colors)
                           )
                ],

                'layout': go.Layout(
                    xaxis={'title': 'Timeline'},
                    yaxis={'title': 'Quantity'},
                    margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
                    legend={'x': 0, 'y': 1},
                    hovermode='closest'
                )
            }
        ), className="four.columns"
        )
    ])

    return html_element


def run_app():
    macron = generate_html("EmmanuelMacron", 1)
    trump = generate_html("BarackObama", 3)

    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

    app.layout = html.Div([
        html.H1(children='Tweet Predictor'),

        html.Div(children='''
            Dash: A web application framework for Python.
        '''),

        dcc.Tabs(id="tabs", value='tab-1', children=[
            dcc.Tab(label='Emmanuel Macron', value='tab-1'),
            dcc.Tab(label='Barack Obama', value='tab-2')]),

        html.Div(id='tabs-content')
    ], className="container")

    @app.callback(Output('tabs-content', 'children'),
                  [Input('tabs', 'value')])
    def render_content(tab):
        if tab == 'tab-1':
            return macron

        elif tab == 'tab-2':
            return trump

    app.run_server(debug=True)


if __name__ == "__main__":
    run_app()
