from twitter_collect.twitter_connection_setup import twitter_setup


def collect(string):
    connexion = twitter_setup()
    return connexion.search(string, language="french", rpp=100)


######################################################################
if __name__ == "__main__":
    string = "Emmanuel Macron"
    list = collect(string)

    for tweet in list:
        print(tweet.text)
