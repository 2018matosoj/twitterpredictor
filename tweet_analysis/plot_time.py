import pandas as pd
import matplotlib.pyplot as plt
from twitter_collect.collect_candidate_tweet_activity import candidate_tweets
from twitter_collect.to_dataframe import transform_to_dataframe


def plot_likes_rts_time(data):
    """
    Plots likes and retweets versus time, of a dataframe
    :param data: dataframe to plot
    :return: nothing
    """
    tfav = pd.Series(data=data['likes'].values, index=data['date'])
    tret = pd.Series(data=data['RTs'].values, index=data['date'])

    # Likes vs retweets visualization:
    tfav.plot(figsize=(16,4), label="Likes", legend=True)
    tret.plot(figsize=(16,4), label="Retweets", legend=True)

    plt.show()


if __name__ == "__main__":
    results = candidate_tweets("EmmanuelMacron")
    data = transform_to_dataframe(results)
    plot_likes_rts_time(data)
