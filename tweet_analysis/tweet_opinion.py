import re
from textblob import TextBlob
from twitter_collect.collect_candidate_actuality_tweets import get_candidate_queries, \
    get_tweets_from_candidates_search_queries
from twitter_collect.collect_candidate_tweet_activity import candidate_tweets
from twitter_collect.to_dataframe import transform_to_dataframe
from twitter_collect.twitter_connection_setup import twitter_setup


def analyze_tweet(text):
    '''
    Analyze a text and identifies if it's positive, negative or neutral
    :param text: String with the text to be analyzed
    :return: returns 1 for positive, 0 for neutral and -1 for negative
    '''
    text = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', text)
    text = text.replace('RT', '')
    text = text.replace('\n', ' ')
    text_blob = TextBlob(text)

    pol = text_blob.sentiment.polarity

    if pol >= 0.3:
        return 1
    elif pol <= -0.3:
        return -1
    else:
        return 0


def analyze_opinion(dataframe):
    '''
    Calculates how many tweets are positive, negative or neutral, given a database
    :param dataframe: database with the tweets
    :return: Three lists: positive tweets, neutral tweets and negative tweets
    '''
    pos_tweets = []
    neu_tweets = []
    neg_tweets = []

    for data in dataframe["tweet_textual_content"]:
        opinion = analyze_tweet(data)

        if opinion == 0:
            neu_tweets.append(data)
        elif opinion == 1:
            pos_tweets.append(data)
        if opinion == -1:
            neg_tweets.append(data)

    return pos_tweets, neu_tweets, neg_tweets


if __name__ == "__main__":
    num_candidate = 1
    queries = get_candidate_queries(num_candidate, "../CandidateData")
    api = twitter_setup()
    results = get_tweets_from_candidates_search_queries(queries, api)
    data_frame = transform_to_dataframe(results)

    pos_tweets, neu_tweets, neg_tweets = analyze_opinion(data_frame)

    print("Percentage of positive tweets: \t{:.2f}%".format(len(pos_tweets)*100/len(data_frame['tweet_textual_content'])))
    print("Percentage of neutral tweets: \t{:.2f}%".format(len(neu_tweets)*100/len(data_frame['tweet_textual_content'])))
    print("Percentage de negative tweets: \t{:.2f}%".format(len(neg_tweets)*100/len(data_frame['tweet_textual_content'])))
    print(neg_tweets)
